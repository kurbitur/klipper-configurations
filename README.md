# Klipper Configurations for Sidewinder X1 with BL-Touch

In this repository you will find two Klipper configurations for the Sidewinder X1. One is for the stock extruder with a BL-touch and the other one is for the Hemera with a BL-touch. I use the Hemera now so it is unlikely that the stock configuration will change much but when I get around to cleaning up the Hemera config I will add relevant changes to the stock config aswell. If you see any issues with my config or have any imporvements to point out feel free to do so.

To use the configs, simply copy and paste the content of the file into your printer.cfg file or download the file and rename it to printer.cfg
