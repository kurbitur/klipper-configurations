[stepper_x]
step_pin: ar54
dir_pin: ar55
enable_pin: !ar38
step_distance: .0125
endstop_pin: ^!ar3
#endstop_pin: ^ar2
position_endstop: -2.5
position_max: 300
position_min: -2.5
homing_speed: 40

[stepper_x]
uart_pin: ar63
tx_pin: ar40
microsteps: 16
run_current: 1.2
hold_current: 0.7

[stepper_y]
step_pin: ar60
dir_pin: ar61
enable_pin: !ar56
step_distance: .0125
endstop_pin: ^!ar14
#endstop_pin: ^ar15
position_endstop: -7
position_max: 340
position_min: -7
homing_speed: 40
homing_positive_dir: false

[stepper_y]
uart_pin: ar64
tx_pin: ar59
microsteps: 16
run_current: 1.2
hold_current: 0.7

[stepper_z]
step_pin: ar46
dir_pin: !ar48
enable_pin: !ar62
step_distance: .0025
endstop_pin: probe:z_virtual_endstop
#endstop_pin: ^!ar18
#endstop_pin: ^ar19
position_endstop: 0.0
position_max: 400
position_min: -3
homing_speed: 5
second_homing_speed: 1
#homing_retract_dist: 0.0

[stepper_z1]
step_pin: ar36
dir_pin: !ar34
enable_pin: !ar30
step_distance: .0025

[z_tilt]
z_positions:
 350,188
 -50,188
points:
 22,83
 222,83
speed: 100
horizontal_move_z: 10
retries: 5
retry_tolerance: 0.01

[extruder]
step_pin: ar26
dir_pin: !ar28
enable_pin: !ar24
step_distance: 0.00244498777
nozzle_diameter: 0.4
filament_diameter: 1.750
heater_pin: ar10
sensor_type: ATC Semitec 104GT-2
sensor_pin: analog13
#control: pid
#pid_Kp=11.65
#pid_Ki=0.84
#pid_Kd=40.56
min_temp: 5
max_temp: 280
pressure_advance: 0.09
pressure_advance_lookahead_time: 0.25
pressure_advance_smooth_time: 0.04
max_extrude_only_distance: 100

[extruder]
uart_pin: ar66
tx_pin: ar44
microsteps: 16
run_current: 0.6
hold_current: 0.2

[heater_bed]
heater_pin: ar8
sensor_type: EPCOS 100K B57560G104F
sensor_pin: analog14
#control: pid
#pid_Kp=34.34
#pid_Ki=5.24
#pid_Kd=150.06
min_temp: 5
max_temp: 120

[fan]
pin: ar9
kick_start_time: 0.500

[heater_fan my_nozzle_fan]
pin:ar7
heater: extruder
heater_temp: 50.0
fan_speed: 1.0

[mcu]
serial: /dev/serial/by-id/usb-1a86_USB_Serial-if00-port0
pin_map: arduino

[printer]
kinematics: cartesian
max_velocity: 200
max_accel: 3000
max_z_velocity: 50
max_z_accel: 100
square_corner_velocity: 5 

[bltouch]
sensor_pin: ar19 
control_pin: ar11 
x_offset:-3
y_offset:27
#z_offset: 0
speed:5
samples:3
samples_result:average

[safe_z_home]
home_xy_position: 153, 123
speed: 100
z_hop: 20               
z_hop_speed: 20

[bed_mesh]
speed: 120
mesh_min: 30,30
mesh_max: 270,270
probe_count: 8,8
mesh_pps: 2,2
algorithm: bicubic
bicubic_tension: 0.2
move_check_distance: 3.0
split_delta_z: .010
fade_start: 1.0 
fade_end: 5.0

[bed_screws]
screw1: 50,50
screw1_name: front left
screw2: 250,50
screw2_name: front right 
screw3: 250,250
screw3_name: back right 
screw4: 50,250
screw4_name: back left
speed: 100.0

[screws_tilt_adjust]
screw1: 22,83
screw1_name: front left
screw2: 222,83
screw2_name: front right 
screw3: 222,283
screw3_name: back right 
screw4: 22,283
screw4_name: back left
speed: 100.0
screw_thread: CW-M5

[gcode_macro G29]
gcode:
 BED_MESH_CLEAR
 G28
 BED_MESH_CALIBRATE
 BED_MESH_PROFILE SAVE=x1
 G90
 G1 X0 Y0 Z5 F4000

[gcode_macro START_PRINT]
# Reference https://shorturl.at/intuA
# On how to override default parameters
default_parameter_BED_TEMP: 60
default_parameter_EXTRUDER_TEMP: 200

gcode:
    # Home the printer
    G28
    # Load bed mesh profile
    BED_MESH_PROFILE LOAD=x1
    # Use absolute coordinates
    G90
    # Move the nozzle near the bed
    G1 X0 Y0 Z5 F3000
    # Move the nozzle very close to the bed
    G1 Z0.15 F300
    # Wait for bed to reach temperature
    M190 S{BED_TEMP}
    # Set and wait for nozzle to reach temperature
    M109 S{EXTRUDER_TEMP}
    G92 E0 ;Reset Extruder
    G1 Z1.0 F3000 ; Move z up little to prevent scratching of surface
    G1 X2 Y20 Z0.3 F5000.0 ; Move to start-line position
    G1 Z0.2 E8 ; Purge Bubble
    G1 X2 Y200.0 Z0.3 F1500.0 E15 ; draw 1st line
    G1 X2 Y200.0 Z0.3 F5000.0 ; move to side a little
    G1 X2 Y20 Z0.3 F1500.0 E30 ; draw 2nd line
    G21 ; set units to millimeters
    G90 ; use absolute coordinates
    M82 ; use absolute distances for extrusion
    G92 E0

[gcode_macro END_PRINT]
gcode:
    # Turn off bed, extruder, and fan
    M140 S0
    M104 S0
    M106 S0
    # Move nozzle away from print while retracting
    G91
    G1 X-2 Y-2 E-3 F300
    # Raise nozzle by 10mm
    G1 Z30 F3000
    G90
    # Disable steppers
    M84


#[display]
#lcd_type: st7920
#cs_pin: ar27
#sclk_pin: ar25
#sid_pin: ar29
#encoder_pins: ^ar23, ^ar17
#click_pin: ^!ar35

#*# <---------------------- SAVE_CONFIG ---------------------->
#*# DO NOT EDIT THIS BLOCK OR BELOW. The contents are auto-generated.
#*#
#*# [extruder]
#*# control = pid
#*# pid_kp = 13.766
#*# pid_ki = 0.454
#*# pid_kd = 104.278
#*#
#*# [heater_bed]
#*# control = pid
#*# pid_kp = 50.480
#*# pid_ki = 0.591
#*# pid_kd = 1077.124
#*#
#*# [bltouch]
#*# z_offset = 2.3
#*#
#*# [bed_mesh default]
#*# version = 1
#*# points =
#*# 	  0.205000, 0.163333, 0.124167, 0.105000, 0.096667, 0.090000, 0.115833, 0.117500
#*# 	  0.177500, 0.135833, 0.089167, 0.064167, 0.044167, 0.031667, 0.054167, 0.045000
#*# 	  0.150833, 0.098333, 0.050833, 0.023333, 0.005833, -0.002500, 0.012500, 0.008333
#*# 	  0.160833, 0.101667, 0.057500, 0.020833, 0.001667, -0.010000, 0.002500, -0.002500
#*# 	  0.155000, 0.088333, 0.034167, 0.000000, -0.021667, -0.043333, -0.030000, -0.030000
#*# 	  0.179167, 0.112500, 0.058333, 0.015000, -0.015833, -0.043333, -0.035833, -0.043333
#*# 	  0.254167, 0.185000, 0.124167, 0.079167, 0.043333, 0.018333, 0.022500, 0.022500
#*# 	  0.240833, 0.170833, 0.100833, 0.045833, 0.008333, -0.023333, -0.017500, -0.026667
#*# x_count = 8
#*# y_count = 8
#*# mesh_x_pps = 2
#*# mesh_y_pps = 2
#*# algo = bicubic
#*# tension = 0.2
#*# min_x = 30.0
#*# max_x = 269.9625
#*# min_y = 30.0000000001
#*# max_y = 269.9625
#*#
#*# [bed_mesh x1]
#*# version = 1
#*# points =
#*# 	  0.205000, 0.163333, 0.124167, 0.105000, 0.096667, 0.090000, 0.115833, 0.117500
#*# 	  0.177500, 0.135833, 0.089167, 0.064167, 0.044167, 0.031667, 0.054167, 0.045000
#*# 	  0.150833, 0.098333, 0.050833, 0.023333, 0.005833, -0.002500, 0.012500, 0.008333
#*# 	  0.160833, 0.101667, 0.057500, 0.020833, 0.001667, -0.010000, 0.002500, -0.002500
#*# 	  0.155000, 0.088333, 0.034167, 0.000000, -0.021667, -0.043333, -0.030000, -0.030000
#*# 	  0.179167, 0.112500, 0.058333, 0.015000, -0.015833, -0.043333, -0.035833, -0.043333
#*# 	  0.254167, 0.185000, 0.124167, 0.079167, 0.043333, 0.018333, 0.022500, 0.022500
#*# 	  0.240833, 0.170833, 0.100833, 0.045833, 0.008333, -0.023333, -0.017500, -0.026667
#*# x_count = 8
#*# y_count = 8
#*# mesh_x_pps = 2
#*# mesh_y_pps = 2
#*# algo = bicubic
#*# tension = 0.2
#*# min_x = 30.0
#*# max_x = 269.9625
#*# min_y = 30.0000000001
#*# max_y = 269.9625
